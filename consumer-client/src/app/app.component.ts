import { Component, OnInit, OnChanges, SimpleChanges, ChangeDetectorRef, ViewContainerRef, ChangeDetectionStrategy } from '@angular/core';
import { FlowService } from './flow.service';
import { Observable } from 'rxjs';
import { Message } from './message.model';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  observable: Observable<Message[]>;

  channelName: string;


  counter: number;
  constructor(private messageService: FlowService, private cd: ChangeDetectorRef, private container: ViewContainerRef) {

  }
  ngOnInit(): void {





  }

  loadMessage() {
    if (this.channelName) {
      this.observable = this.messageService.getMessagesFlow(this.channelName)
        .pipe(map((messages: Message[]) => {
          this.cd.detectChanges();
          return messages;

        }));
    }
  }
}
