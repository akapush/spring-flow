import { Injectable } from '@angular/core';


import { HttpClient, } from '@angular/common/http';
import { Message } from './message.model';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlowService {


  url = 'http://localhost:4200/api/messages/';

  constructor(private httpClient: HttpClient) { }
  getMessagesFlow(channelName: string): Observable<Message[]> {

    return Observable.create((observer) => {
      const messages = new Array();

      const eventSource = new EventSource(this.url + channelName);
      eventSource.onmessage = (event) => {

        const message = JSON.parse(event.data);
        console.log('Received message: ', message);
        messages.push(new Message(message['timestamp'], message['content']));
        observer.next(messages);
      };
      eventSource.onerror = (error) => {
        // readyState === 0 (closed) means the remote source closed the connection,
        // so we can safely treat it as a normal situation. Another way of detecting the end of the stream
        // is to insert a special element in the stream of events, which the client can identify as the last one.
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      };
    });

    /*const eventSource = new EventSource('http://localhost:8081/api/messages/all');
    return new Observable((observer) => {
      eventSource.onmessage = (event) => {
        const data: Message = JSON.parse(event['data']);
        observer.next(data);
      };
    }
    );*/
  }

}
