export class Message {

    constructor(timestamp: number, content: string) {
        this.timestamp = timestamp;
        this.content = content;
    }

    timestamp: number;
    content: string;
    channel: string;
}
