package com.akapush.flowpoc.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlowPocConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlowPocConsumerApplication.class, args);
	}

}
