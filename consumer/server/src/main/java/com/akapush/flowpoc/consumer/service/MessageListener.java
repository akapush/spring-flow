package com.akapush.flowpoc.consumer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.akapush.flowpoc.consumer.dao.MessageRepository;
import com.akapush.flowpoc.consumer.model.Message;
import com.akapush.flowpoc.consumer.stream.MessageStreams;

import lombok.extern.slf4j.Slf4j;

@Component

@Slf4j

public class MessageListener {

	@Autowired
	private MessageRepository messageRespository;

	@StreamListener(MessageStreams.INPUT)
	public void handleMessage(@Payload Message message) {
		log.info("Received messages toto : {}", message);
		messageRespository.save(message).block();

	}

}