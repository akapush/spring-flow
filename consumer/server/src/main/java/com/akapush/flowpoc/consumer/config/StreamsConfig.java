package com.akapush.flowpoc.consumer.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.akapush.flowpoc.consumer.stream.MessageStreams;

@EnableBinding(MessageStreams.class)
public class StreamsConfig {

}