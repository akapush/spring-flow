package com.akapush.flowpoc.consumer.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Document(collection = "messages")
public class Message {

	public Message() {

	}

	public Message(long timestamp, String content, String channel) {
		this.timestamp = timestamp;
		this.content = content;
		this.channel = channel;
	}

	private long timestamp;

	private String content;

	private String channel;
}