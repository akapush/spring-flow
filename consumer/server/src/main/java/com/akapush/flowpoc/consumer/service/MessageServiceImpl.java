package com.akapush.flowpoc.consumer.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;

import com.akapush.flowpoc.consumer.dao.MessageRepository;
import com.akapush.flowpoc.consumer.model.Message;

import reactor.core.publisher.Flux;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private ReactiveMongoTemplate reactiveMongoTemplate;

	@Autowired
	private MessageRepository messageRepository;

	@PostConstruct
	public void init() {
		reactiveMongoTemplate.dropCollection("messages").then(reactiveMongoTemplate.createCollection("messages",
				CollectionOptions.empty().capped().size(2048).maxDocuments(10000))).subscribe();
	}

	@Override
	public Flux<Message> getAllMessages(String channel) {
		return messageRepository.findByChannel(channel);
	}
}
