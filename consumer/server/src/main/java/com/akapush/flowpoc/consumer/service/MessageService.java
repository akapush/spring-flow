package com.akapush.flowpoc.consumer.service;

import com.akapush.flowpoc.consumer.model.Message;

import reactor.core.publisher.Flux;

public interface MessageService {

	Flux<Message> getAllMessages(String channel);
}
