package com.akapush.flowpoc.consumer.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akapush.flowpoc.consumer.model.Message;
import com.akapush.flowpoc.consumer.service.MessageService;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api")
public class MessageController {

	@Autowired
	private MessageService messageService;

	@GetMapping(value = "/messages/{channel}", produces = { MediaType.TEXT_EVENT_STREAM_VALUE })
	public Flux<Message> getAllMessages(@PathVariable(name = "channel") String channel) {

		Flux<Message> messages = messageService.getAllMessages(channel);
		messages.doOnEach(System.out::println);
		return messages;
	}

}
