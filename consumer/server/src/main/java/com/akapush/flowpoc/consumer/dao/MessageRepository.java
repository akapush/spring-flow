package com.akapush.flowpoc.consumer.dao;

import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.akapush.flowpoc.consumer.model.Message;

import reactor.core.publisher.Flux;

public interface MessageRepository extends ReactiveCrudRepository<Message, String> {

	@Tailable
	Flux<Message> findByChannel(String channel);
}
