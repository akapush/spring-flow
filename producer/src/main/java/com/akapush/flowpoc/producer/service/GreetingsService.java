package com.akapush.flowpoc.producer.service;

import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import com.akapush.flowpoc.producer.model.Message;
import com.akapush.flowpoc.producer.stream.MessageStreams;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GreetingsService {

	private final MessageStreams messageStreams;

	public GreetingsService(MessageStreams greetingsStreams) {

		this.messageStreams = greetingsStreams;

	}

	public void sendGreeting(final Message message) {
		log.info("Sending message {}", message);
		MessageChannel messageChannel = messageStreams.outboundMessage();
		messageChannel.send(MessageBuilder.withPayload(message)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON).build());
	}
}