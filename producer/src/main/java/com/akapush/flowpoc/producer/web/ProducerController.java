package com.akapush.flowpoc.producer.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.akapush.flowpoc.producer.model.Message;
import com.akapush.flowpoc.producer.service.GreetingsService;

@RestController

public class ProducerController {

	private final GreetingsService greetingsService;

	public ProducerController(GreetingsService greetingsService) {
		this.greetingsService = greetingsService;
	}

	@GetMapping("/messages")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void greetings(@RequestParam("content") String content, @RequestParam("channel") String channel) {
		Message message = Message.builder().content(content).channel(channel).timestamp(System.currentTimeMillis())
				.build();
		greetingsService.sendGreeting(message);

	}

}