package com.akapush.flowpoc.producer.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface MessageStreams {

	String INPUT = "message-in";

	String OUTPUT = "message-out";

	/*
	 * @Input(INPUT) SubscribableChannel inboundMessage();
	 */

	@Output(OUTPUT)
	MessageChannel outboundMessage();

}