package com.akapush.flowpoc.producer.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.akapush.flowpoc.producer.stream.MessageStreams;

@EnableBinding(MessageStreams.class)
public class StreamsConfig {

}