package com.akapush.flowpoc.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlowPocProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlowPocProducerApplication.class, args);
	}

}
